#!/usr/bin/env perl

use warnings;
use strict;

use English qw( -no_match_vars );
use File::Find;
use File::Temp ();
use Git;
use String::ShellQuote;
use LWP::Simple;

$OUTPUT_AUTOFLUSH = 1;

my $version = shift
    or die "A version is required";

my $repo = Git->repository();

my $temp = File::Temp->new( suffix => '.rpm' );
my $rpm_url = "http://yum.opennms.org/stable/common/opennms/opennms-core-${version}.noarch.rpm";

print "Fetching $rpm_url => $temp... ";
getstore($rpm_url, $temp->filename);
print "done.\n";

print "Cleaning working directory... ";

$repo->command('checkout', 'pristine');
$repo->command('clean', '-dfx');

print "done.\n";

print "Extracting pristine etc files... ";

my $tempdir = File::Temp->newdir();
system "cd $tempdir; " . shell_quote('rpm2cpio', $temp->filename) . ' | ' . shell_quote('cpio', '-i', '-d')
    and die "Failed to extract pristine etc files";

print "done.\n";

print "Merging new pristine etc files... ";

system 'rsync', '-rv', '--exclude=.git*', '--exclude=scripts/', '--del', "$tempdir/opt/opennms/etc/", "."
    and die "Failed to merge pristine etc files";

print "done.\n";

print "Committing... ";

$repo->command('add', '-A', '.');
my $msg = "Pristine files for OpenNMS ${version}";
$repo->command('commit', '-m', $msg);
$repo->command('tag', '-a', '-m', $msg, "pristine-${version}");

print "done.\n";
